# My Portfolio Website

### Check it out here! ~ [kylelynch.me](https://kylelynch.me)

###### Deployment status ~ [![Netlify Status](https://api.netlify.com/api/v1/badges/11d7f5a4-e2e6-4c0f-8da2-67326ecf7a94/deploy-status)](https://app.netlify.com/sites/kyles-updated-portfolio/deploys)


I revised and updated this code, moving from Astro version 1.x to version 3.0, implementing the view transitions API and changing to TailwindCSS from Open Props CSS library.



