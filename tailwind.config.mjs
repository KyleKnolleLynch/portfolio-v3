/** @type {import('tailwindcss').Config} */
import tailwindTypography from '@tailwindcss/typography'
export default {
    content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
    theme: {
        extend: {
            fontFamily: {
                mplusrounded1c: ['MPLUSRounded1c', 'sans-serif'],
            },
            colors: {
                content: 'hsl(var(--clr-content) / <alpha-value>)',
                bgclr: 'hsl(var(--clr-bgclr) / <alpha-value>)',
                primary: 'hsl(var(--clr-primary) / <alpha-value>)',
                secondary: 'hsl(var(--clr-secondary) / <alpha-value>)',
                accent: 'hsl(var(--clr-accent) / <alpha-value>)',
            },
        },
    },
    plugins: [tailwindTypography],
}
