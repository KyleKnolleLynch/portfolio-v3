---
layout: ../layouts/UsesLayout.astro
title: "Kyle Uses"
---
# Things I use

## Languages / Frameworks / Libraries

Javascript / Typescript

HTML / JSX

React / Next.js

CSS / SCSS

TailwindCSS

Astro

Node.js

## Other Dev Software

NPM / pNPM

Git

Code-OSS (open source fork of Visual Studio Code)

Kitty terminal

Chromium Browser/Google Chrome (Devtools)

Firefox/Librewolf

## Graphics / Video

Gimp

Kdenlive

OBS Studio

## Computer

Arch Linux (Arcolinux w/ Hyprland)

Windows

## Code-OSS Theme

Editor Font: JetBrainsMono Nerd Font

Termial Font: MesloLGSDZ Nerd Font Mono

Color Theme: Kanagawa dragon/black

File Icon Theme: Nomo Dark Extended

<br>